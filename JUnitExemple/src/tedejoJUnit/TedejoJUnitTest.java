package tedejoJUnit;

import static org.junit.Assert.*;

import org.junit.Test;

public class TedejoJUnitTest {

	@Test
	public final void testRevertides() {
		assertEquals(true,TedejoJUnit.revertides("tedejo", "jodete"));
		assertEquals(true,TedejoJUnit.revertides("paco", "copa"));
		assertEquals(true,TedejoJUnit.revertides("aaaaaa", "aaaaaa"));
		assertEquals(false,TedejoJUnit.revertides("zz", "zzz"));
		assertEquals(false,TedejoJUnit.revertides("abc", "abc"));
		assertEquals(false,TedejoJUnit.revertides("ab", "ba"));
		assertEquals(false,TedejoJUnit.revertides("abcd", "abcd"));
		assertEquals(false,TedejoJUnit.revertides("aaa", "aaa"));
		assertEquals(true,TedejoJUnit.revertides("aaaa", "aaaa"));
	}

}
