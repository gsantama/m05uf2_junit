//problema 18_I de la CodeJam Febrer 2018

package tedejoJUnit;

import java.util.Scanner;

public class Tedejo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int vegades;
		String paraula1=null;
		String paraula2=null;
		boolean resposta;
		
		vegades = input.nextInt();
		for (int i=0;i<vegades;i++) {
			paraula1 = input.next();
			paraula2 = input.next();
			
		    resposta = revertides(paraula1, paraula2);
		    if (resposta) 
		    	System.out.println("SI");
		    else
		    	System.out.println("NO");
		    		    
		}
		input.close();
	}
	
	private static boolean revertides (String paraula1, String paraula2) {
		String paraulaAux=null;
		boolean rev;
		if (paraula1.length()%2!=0 || paraula2.length()%2!=0 || paraula1.length()!=paraula2.length())
			rev = false;
			else {  
				//invertir paraula1 x parelles
				paraulaAux = "";
				for (int j=paraula1.length()-1;j>=0;j-=2) {
					paraulaAux = paraulaAux + paraula1.charAt(j-1);
					paraulaAux = paraulaAux + paraula1.charAt(j);
				}	

				rev =  paraulaAux.equals(paraula2);
		}
		return rev;
	}
	
}