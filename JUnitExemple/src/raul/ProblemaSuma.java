
//idees: la tenda que m�s ha venut
//m�xim de cada botiga
//quants cotxtes ha venut una botiga
//maxim valor

package raul;

import java.util.Scanner;

public class ProblemaSuma {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int ncasos = sc.nextInt();

		for (int i = 0; i < ncasos; i++) {
			int cotxes = sc.nextInt();
			int tendes = sc.nextInt();
			int[][] mat = new int[cotxes][tendes];

			for (int j = 0; j < cotxes; j++) {
				for (int k = 0; k < tendes; k++) {
					mat[j][k] = sc.nextInt();
				}
			}
		
			/*
			 * exemple:  cotxes 2   tendes 3
			 *              c1t1   c1t2  c1t3
			 *              c2t1   c2t2  c2t3
			 * 
			 */
			
			// TODO Auto-generated method stub
			int maxim = 0;
			for (int j = 0; j < tendes; j++) {
				int acc = 0;
				for (int k = 0; k < cotxes; k++) {
					if (maxim < mat[k][j])
						maxim = mat[k][j];
				}
			}
			System.out.println(maxim);
		}
		return;
	}
}


