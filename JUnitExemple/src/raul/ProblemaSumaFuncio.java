
//idees: la tenda que m�s ha venut
//m�xim de cada botiga
//--> quants cotxtes ha venut una botiga

package raul;

import java.util.Scanner;

public class ProblemaSumaFuncio {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int ncasos = sc.nextInt();
		int valor;
		
	
	    
		for (int i = 0; i < ncasos; i++) {
			int cotxes = sc.nextInt();
			int tendes = sc.nextInt();
			int[][] mat = new int[cotxes][tendes];

			for (int j = 0; j < cotxes; j++) {
				for (int k = 0; k < tendes; k++) {
					mat[j][k] = sc.nextInt();
				}
			}

			/* here */			
			valor = funcion (mat, cotxes, tendes);		
			System.out.println(valor);
		}
		return;
	}
	
	static int funcion(int[][] mat, int cotxes, int tendes) {
		int maxim = 0;
		for (int j = 0; j < tendes; j++) {
			for (int k = 0; k < cotxes; k++) {
				if (maxim < mat[k][j])
					maxim = mat[k][j];
			}
		}
		return maxim;
	}
}


