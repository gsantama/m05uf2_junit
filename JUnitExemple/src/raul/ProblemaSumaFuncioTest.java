package raul;

import static org.junit.Assert.*;

import org.junit.Test;

import lordJUnit.LordF;

public class ProblemaSumaFuncioTest {

	@Test
	public void test() {
	    int [][]mat2 = {{1,2,3},{4,5,6}};
		assertEquals(6,ProblemaSumaFuncio.funcion(mat2, 2, 3));
		
	    int [][]mat3 = {{10,10,40},{20,40,30}};
		assertEquals(40,ProblemaSumaFuncio.funcion(mat3, 2, 3));

	    int [][]mat4 = {{10,10,10},{10,10,10}};
		assertEquals(10,ProblemaSumaFuncio.funcion(mat4, 2, 3));

	    int [][]mat5 = {{10,4,10},{10,10,10}};
		assertEquals(10,ProblemaSumaFuncio.funcion(mat5, 2, 3));

	    int [][]mat6 = {{10,14,10},{10,10,10}};
		assertEquals(14,ProblemaSumaFuncio.funcion(mat6, 2, 3));

	    int [][]mat7 = {{10,8,6},{4,2,1}};
		assertEquals(10,ProblemaSumaFuncio.funcion(mat7, 2, 3));

	}

}
