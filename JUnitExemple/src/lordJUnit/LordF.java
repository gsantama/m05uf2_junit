package lordJUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class LordF {


		public static void main(String[] args) {
			
			Scanner sc = new Scanner(System.in);
			
			int casos = sc.nextInt();
			sc.nextLine();
			
			for (int i = 0; i < casos; i++) {
				String frase = sc.nextLine();
				String frase2 = sc.nextLine();

				boolean lord = islord(frase, frase2);
				if(lord) {
					System.out.println("SI");
				}else {
					System.out.println("NO");
				}
			}
		}

		public static boolean islord(String frase, String frase2) {
			frase = frase.replace(" ", "");
			ArrayList<String> llista = new ArrayList<String>();
			
			for (int j = 0; j < frase.length(); j++) {
				String letra = Character.toString(frase.charAt(j));
				llista.add(letra);
			}
			
			Collections.sort(llista);
			
			frase2 = frase2.replace(" ", "");
			ArrayList<String> llista2 = new ArrayList<String>();
			
			for (int j = 0; j < frase2.length(); j++) {
				String letra = Character.toString(frase2.charAt(j));
				llista2.add(letra);
			}
			
			Collections.sort(llista2);
			
			if (llista.equals(llista2))
				return true;
			else
				return false;

		}
	}