package lordJUnit;
import org.junit.Test;

import tedejoJUnit.TedejoJUnit;

import static org.junit.Assert.*;

public class LordJUnit {

		@Test
		public final void testLord() {
			assertEquals(true,LordF.islord("hhh", "hhh"));
			assertEquals(true,LordF.islord("afdg", "adfg"));
			assertEquals(true,LordF.islord("hola", "hola"));  
			assertEquals(true,LordF.islord("pablo neruda", "nobel para ud"));
			assertEquals(false,LordF.islord("kernet the cat", "leather neck"));
			assertEquals(true,LordF.islord("hola", "hola"));
			assertEquals(true,LordF.islord("hola", "a l o h "));
			assertEquals(false,LordF.islord("aaa", "aaaa"));
			assertEquals(false,LordF.islord("aaaa", "aaa"));
			assertEquals(true,LordF.islord("aa bb cc dd", "abcd abcd"));
			assertEquals(false,LordF.islord("aa bb cc dd", "abcd acdd"));
			assertEquals(false,LordF.islord("qqqq", "qqqp"));
		}

}

