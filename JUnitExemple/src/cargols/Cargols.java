package cargols;

import java.util.Scanner;

public class Cargols {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int prof, puja, baixa;
		int dies;
		int actual;
		int casos = sc.nextInt();
		
		for(int i=0;i<casos;i++) {
			prof = sc.nextInt();
			puja = sc.nextInt();
			baixa = sc.nextInt();
			
			if (prof>puja && puja<=baixa)
				System.out.println("NO");
			else {
				dies = 1;
				actual = puja;
				while (actual<prof) {
					dies ++;
					actual -= baixa;
					actual +=puja;
				}
				System.out.println(dies);
			}	
		}
		sc.close();
	}
}
