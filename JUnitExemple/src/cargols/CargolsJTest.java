package cargols;

import static org.junit.Assert.*;

import org.junit.Test;

public class CargolsJTest {

	@Test
	public void testCargols() {
		assertEquals("3",CargolsJ.cargols(5, 3, 2));
		assertEquals("NO",CargolsJ.cargols(5, 3, 3));
		assertEquals("5",CargolsJ.cargols(5, 1, 0));
		assertEquals("2",CargolsJ.cargols(5, 6, 3));
	}

}
