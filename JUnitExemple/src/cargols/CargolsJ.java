package cargols;

import java.util.Scanner;

public class CargolsJ {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int prof, puja, baixa;
		int casos = sc.nextInt();
		String resposta;

		for (int i = 0; i < casos; i++) {
			prof = sc.nextInt();
			puja = sc.nextInt();
			baixa = sc.nextInt();

			resposta = cargols(prof, puja, baixa);
			System.out.println(resposta);
		}

		sc.close();
	}

	static public String cargols(int prof, int puja, int baixa) {
		int dies;
		int actual;
		String resposta;

		if (prof > puja && puja <= baixa)
			resposta = "NO";
		else {
			dies = 1;
			actual = puja;
			while (actual < prof) {
				dies++;
				actual -= baixa;
				actual += puja;
			}
			;
			resposta = String.valueOf(dies);
		}
		return resposta;
	}
}
